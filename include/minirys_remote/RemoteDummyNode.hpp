#pragma once

#include <rclcpp/rclcpp.hpp>

#include <geometry_msgs/msg/twist.hpp>
#include <minirys_msgs/srv/set_pose.hpp>
#include <nav_msgs/msg/odometry.hpp>
#include <sensor_msgs/msg/battery_state.hpp>
#include <sensor_msgs/msg/imu.hpp>
#include <sensor_msgs/msg/range.hpp>
#include <sensor_msgs/msg/temperature.hpp>
#include <std_msgs/msg/bool.hpp>

class RemoteDummyNode: public rclcpp::Node {
public:
	explicit RemoteDummyNode(rclcpp::NodeOptions options);

private:
	rclcpp::TimerBase::SharedPtr updateTimer;

	rclcpp::Subscription<sensor_msgs::msg::BatteryState>::SharedPtr batteryStateSubscription;
	rclcpp::Subscription<sensor_msgs::msg::Range>::SharedPtr distanceSubscriptions[6];
	rclcpp::Subscription<sensor_msgs::msg::Temperature>::SharedPtr mainTempSubscription;
	rclcpp::Subscription<sensor_msgs::msg::Temperature>::SharedPtr cpuTempSubscription;
	rclcpp::Subscription<sensor_msgs::msg::Imu>::SharedPtr imuSubscription;
	rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr odometryValidSubscription;
	rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr odometrySubscription;

	rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr velocityCommandPublisher;
	rclcpp::Publisher<std_msgs::msg::Bool>::SharedPtr balanceModePublisher;

	rclcpp::Client<minirys_msgs::srv::SetPose>::SharedPtr setPoseClient;

	void update();
};
