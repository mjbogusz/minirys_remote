#include "minirys_remote/RemoteDummyNode.hpp"

#include <chrono>

using namespace std::chrono_literals;

RemoteDummyNode::RemoteDummyNode(rclcpp::NodeOptions options):
	Node("remote_dummy", options) {
	// Set up subscriptions
	this->batteryStateSubscription = this->create_subscription<sensor_msgs::msg::BatteryState>(
		"battery",
		10,
		[](sensor_msgs::msg::BatteryState::SharedPtr /*unused*/) {}
	);
	this->mainTempSubscription = this->create_subscription<sensor_msgs::msg::Temperature>(
		"temperature_main",
		10,
		[](sensor_msgs::msg::Temperature::SharedPtr /*unused*/) {}
	);
	this->cpuTempSubscription = this->create_subscription<sensor_msgs::msg::Temperature>(
		"temperature_cpu",
		10,
		[](sensor_msgs::msg::Temperature::SharedPtr /*unused*/) {}
	);
	this->imuSubscription = this->create_subscription<sensor_msgs::msg::Imu>(
		"imu",
		10,
		[](sensor_msgs::msg::Imu::SharedPtr /*unused*/) {}
	);
	this->odometryValidSubscription = this->create_subscription<std_msgs::msg::Bool>(
		"odom_valid",
		10,
		[](std_msgs::msg::Bool::SharedPtr /*unused*/) {}
	);
	this->odometrySubscription = this->create_subscription<nav_msgs::msg::Odometry>(
		"odom",
		10,
		[](nav_msgs::msg::Odometry::SharedPtr /*unused*/) {}
	);
	for (int i = 0; i < 6; i++) {
		std::string topicName = "distance_" + std::to_string(i);
		this->distanceSubscriptions[i] = this->create_subscription<sensor_msgs::msg::Range>(
			topicName,
			10,
			[](sensor_msgs::msg::Range::SharedPtr /*unused*/) {}
		);
	}

	this->velocityCommandPublisher = this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);
	this->balanceModePublisher = this->create_publisher<std_msgs::msg::Bool>("balance_mode", 10);

	// Set up service client
	this->setPoseClient = this->create_client<minirys_msgs::srv::SetPose>("set_pose");

	// Set up timer for calling the service
	this->updateTimer = this->create_wall_timer(1s, std::bind(&RemoteDummyNode::update, this));
}

void RemoteDummyNode::update() {
	if (!this->setPoseClient->wait_for_service(10ms)) {
		RCLCPP_INFO(this->get_logger(), "service not available, skipping");
		return;
	}

	geometry_msgs::msg::Twist velocityMessage {};
	this->velocityCommandPublisher->publish(velocityMessage);

	std_msgs::msg::Bool balanceModeMessage {};
	this->balanceModePublisher->publish(balanceModeMessage);

	auto request = std::make_shared<minirys_msgs::srv::SetPose::Request>();
	request->pose.orientation.w = 0.0;
	request->pose.orientation.x = 0.0;
	request->pose.orientation.y = 0.0;
	request->pose.orientation.z = 0.0;
	request->pose.position.x = 0.0;
	request->pose.position.y = 0.0;
	request->pose.position.z = 0.0;

	using SetPoseResponseFuture = rclcpp::Client<minirys_msgs::srv::SetPose>::SharedFuture;
	this->setPoseClient->async_send_request(request, [](SetPoseResponseFuture /*unused*/) {});
}
