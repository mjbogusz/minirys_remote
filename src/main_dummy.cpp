#include "minirys_remote/RemoteDummyNode.hpp"

int main(int argc, char const* argv[]) {
	rclcpp::init(argc, argv);
	rclcpp::NodeOptions options;

	auto remoteDummyNode = std::make_shared<RemoteDummyNode>(options);
	rclcpp::spin(remoteDummyNode);
	rclcpp::shutdown();
	return 0;
}
